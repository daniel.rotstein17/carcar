import React from "react";

class NewCustomerForm extends React.Component {
    constructor(){
        super()
        this.state = {
            name: "",
            address: "",
            phone_number: "",
        }
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...this.state};
        const customerUrl = "http://localhost:8090/api/sales/customers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        
        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();
            const cleared = {
                name: "",
                address: "",
                phone_number: "",
            };
            this.setState(cleared);
            const successAlert = document.getElementById("success-message")
            successAlert.classList.remove("d-none")

        };
    }

    handleInputChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;    
        this.setState({...this.state, [name]: value})
    }

    render() {
        return (
            <div className="container pt-5">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add Customer</h1>

                        <form onSubmit={this.handleSubmit} id="create-sales-person-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                                <label htmlFor="Name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} value={this.state.address} placeholder="Address" required type="text" name="address" id="address" className="form-control"/>
                                <label htmlFor="address">Address</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} value={this.state.phone_number} placeholder="Phone Number" required type="text" name="phone_number" id="phone_number" className="form-control"/>
                                <label htmlFor="phone_number">Enter Phone Number</label>
                            </div>
                            <button className="btn btn-success">Add</button>
                        </form>
                        <div className="alert alert-success d-none mt-5" id="success-message">
                            Successfully added a new customer!
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default NewCustomerForm;