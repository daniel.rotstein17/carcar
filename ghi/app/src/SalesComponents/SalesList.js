import React from "react";

class SalesList extends React.Component {
    constructor() {
        super();
        this.state = { sales:[] };
    }

    async componentDidMount() {
        const response = await fetch ("http://localhost:8090/api/sales/")
        if (response.ok) {
            const data = await response.json();
            this.setState({ sales: data.sales });
        } 
        else {
            console.error(response)
        }
    }

    render () {
        return (
            <div className="container pt-5">
                    <h1>Sales History</h1>
                    <div className="input-group mb-5">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales Person</th>
                            <th>Employee Number</th>
                            <th>Purchaser Name</th>
                            <th>VIN</th>
                            <th>Sale Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.sales.map(sale =>{
                            return (
                                <tr key={ sale.id }>
                                    <td>{ sale.sales_person.name }</td>
                                    <td>{ sale.sales_person.employee_number }</td>
                                    <td>{ sale.customer.name }</td>
                                    <td>{ sale.automobile.vin }</td>
                                    <td>${ sale.price }</td>
                                </tr>
                            );
                        })}
                    </tbody>  
                </table>
                </div>
            </div>
        )
    }
}

export default SalesList;