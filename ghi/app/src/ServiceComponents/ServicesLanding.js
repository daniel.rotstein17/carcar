import React from "react";
import { Link } from 'react-router-dom';


class ServicesLanding extends React.Component {

    render() {
        return (
            <div className="container mt-5">
                <div className="row">
                    <div className="row justify-content-evenly">
                        <div className="col-6 col-sm-2 shadow p-1 m-1 text-success bg-white rounded">
                            <h3 className="text-center">Service Appointments</h3>
                            <Link to="/appointments">
                                <img src="/schedule-appointment.jpeg" className="img-fluid" alt="service app"/>
                            </Link>
                        </div>
                        <div className="col-6 col-sm-2 shadow p-1 m-1 text-success bg-white rounded">
                            <h3 className="text-center">Service History</h3>
                            <Link to="/appointments/history">
                                <div className="bg-white">
                                <img src="/history.png" className="img-fluid" alt="service history" />
                                </div>
                            </Link>
                        </div>
                    </div>
                </div>
                <div className="row mt-5">
                    <div className="row justify-content-evenly">
                        <div className="col-6 col-sm-2 shadow p-1 m-1 text-success bg-white rounded">
                            <h3 className="text-center">Schedule an Appointment</h3>
                            <Link to="/appointments/new">
                                <img src="/schedule-app.png" className="img-fluid" alt="schedule app" />
                            </Link>
                        </div>
                            <div className="col-6 col-sm-2 shadow p-1 m-1 text-success bg-white rounded">
                            <h3 className="text-center">Add Technician</h3>
                            <Link to="/technicians">
                                <img src="/mechanic.jpeg" className="img-fluid" alt="add tech" />
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default ServicesLanding;