import React from "react";


class VehicleModelList extends React.Component {
    constructor() {
        super();
        this.state = {
            models: []
        };
    }


    async componentDidMount() {
        const response = await fetch ("http://localhost:8100/api/models/");
        if (response.ok) {
            const data = await response.json();
            this.setState({ models: data.models })
        }
        else {
            console.error(response)
        }
    }

    
    render () {
        return (
            <div className="container pt-5">
                <h1>Vehicle Models</h1>
                <table className="table table-striped mt-5">
                    <thead>
                        <tr>
                            <th>Model</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.models.map(model => {
                                return (
                                    <tr key={ model.id }>
                                        <td>{ model.name }</td>
                                        <td>{ model.manufacturer.name }</td>
                                        <td>
                                            <img src={ model.picture_url } alt="vehicle-model-picture" width="250px"/>
                                        </td>
                                    </tr>
                                );
                            })}
                    </tbody>
                </table>                     
            </div>
        )
    }
}


export default VehicleModelList;