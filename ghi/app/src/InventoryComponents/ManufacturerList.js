import React from "react";


class ManufacturerList extends React.Component {
    constructor() {
        super();
        this.state = {
            manufacturers: []
        };
    }


    async componentDidMount() {
        const response = await fetch ("	http://localhost:8100/api/manufacturers/");
        if (response.ok) {
            const data = await response.json();
            this.setState({ manufacturers: data.manufacturers })
        }
        else {
            console.error(response)
        }
    }

    
    render () {
        return (
            <div className="container pt-5">
                <h1>Manufacturers</h1>
                <table className="table table-striped mt-5">
                    <thead>
                        <tr>
                            <th>Name</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.manufacturers.map(m => {
                                return (
                                    <tr key={m.id }>
                                        <td>{ m.name }</td>
                                    </tr>
                                );
                            })}
                    </tbody>
                </table>                     
            </div>
        )
    }
}


export default ManufacturerList;