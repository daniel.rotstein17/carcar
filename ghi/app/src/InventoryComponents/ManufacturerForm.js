import React from "react";

class ManufacturerForm extends React.Component {
    constructor(){
        super()
        this.state = {
            name: "",            
        }
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...this.state};
        const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        
        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
            const newManufacturer = await response.json();
            const cleared = {
                name: "",
            };
            this.setState(cleared);
            const successAlert = document.getElementById("success-message")
            successAlert.classList.remove("d-none")

        };
    }
    
    handleInputChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;    
        this.setState({...this.state, [name]: value})
    }


    render(){
        return (
            <div className="container pt-5">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a Manufacturer</h1>

                        <form onSubmit={this.handleSubmit} id="create-sales-person-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                                <label htmlFor="Name">Name</label>
                            </div>
                            <button className="btn btn-success">Add</button>
                        </form>
                        <div className="alert alert-success d-none mt-5" id="success-message">
                            Successfully added a new manufacturer!
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ManufacturerForm;