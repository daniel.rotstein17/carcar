import React from "react";


class AutomobileList extends React.Component {
    constructor() {
        super();
        this.state = {
            autos: []
        };
    }


    async componentDidMount() {
        const response = await fetch ("http://localhost:8100/api/automobiles/");
        if (response.ok) {
            const data = await response.json();
            this.setState({ autos: data.autos })
        }
        else {
            console.error(response)
        }
    }

    
    render () {
        return (
            <div>
                <h1>Automobiles</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Color</th>
                            <th>Year</th>
                            <th>Model</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.autos.map(auto => {
                                return (
                                    <tr key={auto.id }>
                                        <td>{ auto.vin }</td>
                                        <td>{ auto.color }</td>
                                        <td>{ auto.year }</td>
                                        <td>{ auto.model.name }</td>
                                        <td>{ auto.model.manufacturer.name }</td>
                                        <td>
                                            <img src={ auto.model.picture_url } alt="auto-pic" width="250px"/>
                                        </td>
                                    </tr>
                                );
                            })}
                    </tbody>
                </table>                     
            </div>
        )
    }
}


export default AutomobileList;