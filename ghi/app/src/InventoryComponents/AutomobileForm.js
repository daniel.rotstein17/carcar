import React from "react";


class AutomobileForm extends React.Component {
    constructor() {
        super();
        this.state = {
            vin: "",
            color: "",
            year: "",
            model_id: "", 
            models: [],
        }
    }


    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...this.state};
        delete data.models;
  
        const automobileUrl = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
             },
        };
        const response = await fetch(automobileUrl, fetchConfig);

        
        if (response.ok) {
            const newAutomobile = await response.json();

            const cleared = {
                vin: "",
                color: "",
                year: "",
                model_id: "",
            };
            this.setState(cleared);
            const successAlert = document.getElementById("success-message")
            successAlert.classList.remove("d-none")
        }
    }


    handleInputChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({...this.state, [name]: value})
    }


    async componentDidMount() {
        const modelsUrl = "http://localhost:8100/api/models/";
        const response = await fetch(modelsUrl);

        if (response.ok) {
            const data = await response.json();
            this.setState({models: data.models})
        }
    }

    
    render() {
        return (
            <div className="container pt-5">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add an Automobile</h1>
                        <form onSubmit={this.handleSubmit} id="create-automobile-form">

                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} value={this.state.vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control"/>
                            <label htmlFor="vin">VIN</label>
                        </div>


                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} value={this.state.color} placeholder="Color<" required type="text" name="color" id="color" className="form-control"/>
                            <label htmlFor="color">Color</label>
                        </div>


                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} value={this.state.year} placeholder="Year" required type="number" name="year" id="year" className="form-control"/>
                            <label htmlFor="year">Year</label>
                        </div>

                      
                        <div className="mb-3">
                            <select onChange={this.handleInputChange} value={this.state.model_id} required id="model_id" name="model_id" className="form-select">
                            <option value="">Choose a Model</option>

                            {this.state.models.map(model_id => {
                                return (
                                    <option key={model_id.id} value={model_id.id}>
                                        {model_id.manufacturer.name} {model_id.name}
                                    </option>
                                );
                            })}
                            </select>
                        </div>
                        <button className="btn btn-success">Add</button>
                        </form>
                        <div className="alert alert-success d-none mt-5" id="success-message">
                            Successfully added a new automobile to inventory!
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default AutomobileForm;