from django.urls import path

from .views import api_sales_people, api_customers, api_sales

urlpatterns = [
    path("sales/salespeople/", api_sales_people, name='api_salespeople'),
    path("sales/salespeople/<int:employee_number>/", api_sales, name='api_salesfiltered'),
    path("sales/customers/", api_customers, name="api_customers"),
    path("sales/", api_sales, name="api_sales"),
]