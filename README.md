<br />
<div align="center">
  <h1 align="center">CarCar</h1>
</div>

## 🧠 Team members

- Daniel Rotstein
- Joey Salvo

## 💻 About The Project

<p align="center">
    This project is designed to help car dealerships manage their inventory, sales, and service departments.
</p>

### 📌 Inventory

The inventory section was a collaboration between Joey and Daniel.
There are three forms - one to add an automobile, add a manufacturer and another to add a vehicle model.
Also included are three views that allow for viewing ists of manufacturers, vehicle models and an automobile inventory.
There's a search function to filter manufacturers in the Automobile Inventory list.

#### Automobile Inventory

![automobile_inventory.gif](./automobile_inventory.gif)

### 📌 Sales

The sales section was created by Joey. It's designed to provide a comprehensive car selling experience for salespeople.
There are three forms – one to add new customers, add new salespersons and another to record new sales. The sale record form allows salespersons to choose from existing customers, salespeople, and vehicles in inventory and then to provide the price for which the car was sold.
There are also two views that allow for viewing sales history. The first, "Sales History", shows all sales that have been completed. The second, "Individual Performance", has a dropdown to select the history of each salesperson on the team. When no salesperson is selected, the list shows all sales.

### 📌 Service

The service section was created by Daniel. It's designed to keep track of service appointments and appointment history for your service department.
There are two forms - one to allow your service department to easily schedule service appointments, and another to add a new technician.
The appointment list allows you to keep track of all the upcoming service appointments, track the VIP status of your customers (those who have purchased a vehicle from your dealership), as well as buttons to "finish" or "cancel" appointments.
Also included is an appointment history list that provides an easy way to show completed appointments. There is a live search feature on this list that allows you to easily search by VIN and customer name to see completed appointments on a specific vehicle.

#### Service History Live Search

![service_history_live_search.gif](./service_history_live_search.gif)

### 🎨 Styling

For styling we used Bootstrap and also CSS to customize the design.

### 🛠 Built With

This page was build with the use of following frameworks/libraries and tools:

- [React][react-url]
- [Django][django-url]
- [Bootstrap][bootstrap-url]
- [Docker][docker-url]

## 🚀 Installation and Setup

To fully enjoy CarCar on your local machine, please make sure to follow these steps:

1. Clone the repository down to your local machine
2. CD into the new project directory
3. Run `docker volume create beta-data`
4. Run `docker compose build`
5. Run `docker compose up`

## 🪪 Contact

Daniel Rotstein - daniel.rotstein17@gmail.com

Joey Salvo - joey.salvo.dev@gmail.com

<h5> Project Link: https://gitlab.com/daniel-rotstein/carcar </h5>

[react-url]: https://reactjs.org/
[django-url]: https://www.djangoproject.com/
[bootstrap-url]: https://getbootstrap.com
[docker-url]: https://docker.com/
